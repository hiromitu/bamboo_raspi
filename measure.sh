#!/bin/bash

set -x

DATETIME=`date "+%Y%m%d_%H%M%S"`
DATE_DAY=`date "+%Y%m%d"`

CO2_TOP=`find /dev/serial/by-path/ -name "*5.2*"`
CO2_BOTTOM=`find /dev/serial/by-path/ -name "*5.1*"`

#read sdi12 value

#read co2 value

LOGPATH=/log/$DATE_DAY/$DATETIME.txt
mkdir /log/$DATE_DAY/
while read line
do
  echo $line
    if [ "`echo $line | grep 'SEN'`" ] ; then
    echo "CO2,"$line > $LOGPATH
    break;
    fi
done < $CO2_TOP

#read one wire temp
find /sys/devices/w1_bus_master1/ -name "temp1*" |  xargs -I{} sh -c 'echo -n 1w,{},; cat {}' >> $LOGPATH
curl --url http://27.134.253.246/bamboo/`cat /etc/hostname`/$DATE_DAY/ -X MKCOL  -u bamboo:bamboo
curl --url http://27.134.253.246/bamboo/`cat /etc/hostname`/$DATE_DAY/$DATETIME.txt -X PUT -T $LOGPATH -u bamboo:bamboo
